runner {
  filterStackTrace false
  optimizeRunOrder
}
spockReports {
    set 'com.athaydes.spockframework.report.showCodeBlocks': true
    set 'com.athaydes.spockframework.report.outputDir': 'build/reports/spock-test'
    set 'com.athaydes.spockframework.report.showCodeBlocks': true
}
