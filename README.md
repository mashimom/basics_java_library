# Basics for a _hit the ground running_ Gradle Java library project

[![pipeline status](https://gitlab.com/mashimom/basics_java_library/badges/master/pipeline.svg)](https://gitlab.com/mashimom/basics_java_library/commits/master)
[![unit test coverage report](https://gitlab.com/mashimom/basics_java_library/badges/master/coverage.svg?job=test)](https://gitlab.com/mashimom/basics_java_library/commits/master)
[![integration test coverage report](https://gitlab.com/mashimom/basics_java_library/badges/master/coverage.svg?job=integrationTest)](https://gitlab.com/mashimom/basics_java_library/commits/master)


> After being tired of going back to old projects in order to configure a new one I decided to build this project as the _current starting state_ of a _very personal and very opinionated_ Java library project.

## Goals

Have a concise starting point for __"my"__ new project without much hassle.  
Find opinionated best practices and make them standard.

## Features

* [x] sensible .gitignore
* [x] sensible .gitattributes
* [x] Based on Gradle init command
```
gradle init --type java-library --test-framework spock
```
* [x] Sensible core Gradle plugins `build-announcements`, `build-dashboard`, `project-report`, `jacoco`
* [x] Sensible Nebula plugins `release`, `project`, `facet`, `source-jar`, `test-jar`, `maven-publish`
* [x] Using Gradle plugin `ben-manes:versions` to keep an eye on dependency versions
* [x] Unit tests and Integration tests as separate sources
* [x] Spock mock's dependencies `cglib`, `objenesis`
* [x] Spock reports (`com.athaydes:spock-reports`)
* [x] Spock config `src\test\resources\SpockConfig.groovy` and `src\integrationTest\resources\SpockConfig.groovy`
* [x] git commit id based versioning
* [x] gitlab pages build/publish for documentation
* [x] gitlab sample build
* [ ] jenkins sample build
* [ ] ~~travis sample build~~
* [ ] ~~circleci sample build~~
* [ ] ~~make it either a PR to Gradle init or find a nice templating engine to use~~
* [ ] use either as lazybones or a custom mustache based template

> light promises
* [ ] Keep it reasonably up-to-date
* [ ] Add best practices
